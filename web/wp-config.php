<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0777 );

define( 'FS_CHMOD_FILE', 0777 );
define( 'DB_NAME', 'fm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'npxVm[iN*Efi%sIxqWS`-&W:1Z1&M/qKW;:_Jd?zkkdT~irPN[j,[[+AeN>8O U=' );
define( 'SECURE_AUTH_KEY',  '0yrxB4.lAW|/_bNn& ik8lz*R|M!9@`DyPzf|VU7_#2S@Sx(bxbNWNI&Z29<^p5d' );
define( 'LOGGED_IN_KEY',    'ku+WPc|oO#`S?%7{dol;~*w?[p0JlCNRnHZCK?`[}Gf1Vz~jlC[ow*m2A=U#Ld27' );
define( 'NONCE_KEY',        '~Cfr@s:7`O^*/f-v[Cm0Pu`~sdt8v=q4DeZ^lM `]0eR_bDAXMQIJnuW`.c5+6:]' );
define( 'AUTH_SALT',        'AAoRvm5B`7y4Iq@y-;1dRjoP,Kk-3|!WQxg;VvLF@MoSEpu2NwNbMp1s(<h>_tY9' );
define( 'SECURE_AUTH_SALT', '>o^CHF&zFcOM6a6;lg/iD0U,{{@QlyPnJ!9)#VTW`,i1x]aak,~Kt*9uhT]e|0D,' );
define( 'LOGGED_IN_SALT',   'CjvQHn_(jLRTCj;Bdxx<p.)$S`^&;iW-._YCDnjwBUBUgoE)P7mF)G`pU:LL,,0J' );
define( 'NONCE_SALT',       'qoyE[gx5k:Yo054u@ba2BkdvdgQ_|?49qZBwN_ 9hh:~w:?g:(J|/^R1%94Qy&5y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
