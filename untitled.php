<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0777 );

define( 'FS_CHMOD_FILE', 0777 );
define( 'DB_NAME', 'ga' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xq_&N68>C iOt7bs~K703i.9MJwdWD_B~hWo/FC!tHvh-dm)>_g[<4xC$7,k- nW' );
define( 'SECURE_AUTH_KEY',  'c;qH&L.p,rE@XKhyo)uplx!TtjbRk]2YHHarrks+_nvwN4c,~zxU|6zr 69f_A_]' );
define( 'LOGGED_IN_KEY',    'o1{Vxu}A{VV2duBeO7Tv.dYlv3S$NhNQx9`Fv<@mz_oYcLPs[@:;!kFtPiu&40/f' );
define( 'NONCE_KEY',        '05IWs^feiSyVLPBXXS@(+qm6vQ$~9 7g:$sIhah]!,>wu++v;,qkq-=OXSQyM #K' );
define( 'AUTH_SALT',        '|YX^misiqBuydBztN3Q,G{m5~}n}?Q8|q[M6|!djeaJT^{)z}j9TfX,@D)p:(#r9' );
define( 'SECURE_AUTH_SALT', 'E`KQHRrhmklD}%ni*Ee%rCG)0q.A>&X>6fU9uCy3W;~Nd0hB*R(;|YVY>9s`>{Qv' );
define( 'LOGGED_IN_SALT',   '$+X<)2R)WDLmak0+GtD}` fdABURm)emN2S42UtNg>KB*-@R`1IY2L;bjGGX{K-&' );
define( 'NONCE_SALT',       'K_Yi.{!)+MZJ;gX-=cIh$=BK~E@&h1ntJ3>S8m(rJ$R[C,fjMR5Rb7$^iI@h<N9k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
